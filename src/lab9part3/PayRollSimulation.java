/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part3;

/**
 *
 * @author dannylowpass
 */
public class PayRollSimulation {
    
    public static void main(String[] args){
    
        EmployeeFactory empFactory = EmployeeFactory.getInstance();

        Employee Greg = new Employee("Greg", 15.0, 40);
        Manager Gonzalo = new Manager("Gonzalo", 20.0, 40, 100);
        
        System.out.println("Greg's pay " + Greg.calculatePay());
        System.out.println("Gonzalo's pay " + Gonzalo.calculatePay());
        
    }
    
}
