/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part3;

/**
 *
 * @author dannylowpass
 */
public class Manager extends Employee {
    
    double bonus;
    
    public Manager(){
        
    }
    
    public Manager(double hourlyWage, double hoursWorked, double bonus){
        super (hourlyWage, hoursWorked);
        this.bonus = bonus;
    }
    
    public Manager(String name, double hoursWorked, double hourlyWage, double bonus){
        super (name, hourlyWage, hoursWorked);
        this.bonus = bonus;
    }
    @Override
    public double calculatePay(){
    
        return super.calculatePay( ) + bonus;

    } 
    
    public void setBonus(double bonus){
        this.bonus = bonus;
    }
}
