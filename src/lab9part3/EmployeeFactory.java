/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part3;

/**
 *
 * @author dannylowpass
 */
public class EmployeeFactory {
    
    private static EmployeeFactory empFactory;
    
    private EmployeeFactory(){};
    
    public static EmployeeFactory getInstance(){
        
        if (empFactory == null){
            empFactory = new EmployeeFactory();
        }return empFactory;
    }
    
    public Employee getEmployee(EmployeeType type, String name, double 
            hourlyWage, double hoursWorked, double bonus){
        
        Employee emp = null;
        
        switch ( type ){
        
        case EMPLOYEE :
            emp = new Employee(name, hourlyWage, hoursWorked);
            break;
            
        case MANAGER : 
            emp = new Manager(name, hourlyWage, hoursWorked, bonus);
            break;
        } 
        return emp;
    }
    
}
